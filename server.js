const express = require("express")
const app = express();

const port = 8000;
const routes = require('./app/routes/api.js')(app, express);

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});


//  Data parsing.
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

//  Setup endpoints for api.
app.use('/', routes);
app.use('/story', routes);

//  Views rendering engine.
app.set('views', __dirname + '/public/views');
app.set('view engine', 'ejs');

app.listen(port, () => console.log("listening on port:" + port));