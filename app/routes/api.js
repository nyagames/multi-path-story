module.exports = (app, express) => {
    const router = express.Router();

    //  Controllers are not setup in this test, will control in here instead.
    // const mainController = require('../../public/controllers/mainController')(app, express);

    const stories = [
        [{
            'sentence': 'Once upon a time, there was a quick brown fox.',
            'isClicked': 'false',
            'page': 1
        }]
    ];

    let pageCounter = stories.length;
    let currentPage = stories.length - 1;
    let currentStory = stories[currentPage];

    const addPage = (page, sentence) => {
        page = [{
            'sentence': sentence,
            'isClicked': 'false',
            'page': pageCounter
        }];

        if (stories.length <= 0) {
            stories[pageCounter] = page;
        } else {
            for (let i = 0; i < stories.length; i++) {
                if (stories[i] !== page) {
                    stories[pageCounter] = page;
                }
            }
        }
    }

    const createPage = sentence => {
        pageCounter = stories.length;
        currentPage = stories.length - 1;
        addPage("page" + pageCounter, sentence);
        pageCounter = stories.length;
        currentPage = stories.length - 1;
    }

    const newPage = sentence => {
        currentPage = stories.length - 1;
        createPage(sentence);
        currentStory = stories[currentPage];
    }

    const addSentence = (pageIndex, index, sentence) => {
        stories[pageIndex][index] = {
            'sentence': sentence,
            'isClicked': 'false',
            'page': currentPage
        }
    }

    const modifySentence = (pageIndex, index, sentence, isClicked, newPage) => {
        stories[pageIndex][index] = {
            'sentence': sentence,
            'isClicked': isClicked,
            'page': newPage
        }
    }


    //  Server


    // Render current story.
    router.get('/', (req, res) => {
        if (res) {
            console.log("api routed succesfully.");

            res.render('index', {
                stories: stories[currentPage],
                pageIndex: currentPage
            });
        }
    });


    //  Back to start link. Redirects to supplied page index. 
    router.get('/back/:index', (req, res) => {
        if (res) {
            const index = parseInt(req.params.index);
            currentPage = index;
            currentStory = stories[currentPage];
            res.render('index', {
                stories: stories[currentPage],
                pageIndex: currentPage
            });
        }
    });


    //  If page exists it will render it, 
    //  otherwise create a new page.
    router.get('/story/:pageIndex/:index/:sentence/:isClicked', (req, res) => {
        if (res) {
            const clicked = req.params.isClicked;
            const index = parseInt(req.params.index);
            const pageIndex = parseInt(req.params.pageIndex);
            const sentence = req.params.sentence;

            if (clicked === "true") {
                currentPage = pageIndex;
                currentStory = stories[currentPage];

                res.render('index', {
                    stories: stories[pageIndex],
                    pageIndex: index
                });
            } else {
                modifySentence(pageIndex, index, sentence, "true", pageCounter);
                newPage(sentence);
                res.render('index', {
                    stories: stories[currentPage],
                    pageIndex: currentPage
                });
            }
        }
    });

    // POST - Add sentence to view.
    router.post('/story/:index', (req, res, next) => {
        if (res) {
            const index = req.params.index;
            const sentence = req.body.sentence;
            if (sentence.length > 0) {
                addSentence(currentPage, index, sentence)
            }
            res.render('index', {
                stories: stories[currentPage],
                pageIndex: currentPage
            });
        }
    });

    return router
}