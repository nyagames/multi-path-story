Instructions

1. Open the "multi-path-story" folder in Visual Studio Code.

2. Run 

   ```bash
   npm install
   ```

   in the terminal to install required node modules found in the package.json file.

3. Run 

   ```bash
   npm start
   ```

   in the terminal, once the required node modules above has installed. 

   It will start the server at port 8000.

   ```
   listening on port:8000
   ```

   The above will appear in the terminal once it has started.

   The application is then accessible at http://localhost:8000/. 

4. Create a multiple path story
   1. Select from any of the available input fields to type in a sentence. Click the **"Submit"** button below it. This will render the sentence to the current page, turning the sentence into a hyperlink text field. This hyperlink allows for creating a new path in the story, and essentially renders a new page with that sentence as the central sentence for that page.
   2. Add more sentences to the page by repeating the above step until there is no further input fields.
   3. Click on any available sentences that were submitted to continue on from that sentence on a different page. 
   4. Select **"Back to the start"** anytime to return to the first page to review the story created so far and/or to build more paths of the story.

--------------------

Development

Technology used: JavaScript, expressJS, nodeJS, and eJS.



For quick HTML markup and condition checks, I used eJS template engine as it uses all the JavaScript logic and the syntax is an expansion of HTML, which is something many developers are already familiar working with. This makes it easier to distribute code amongst a team. PUG is another view engine I could have used, the syntax is simplified to make it more readable but since it is simplified some time might be required to learn the changes.

I initially set out to use a database such as MongoDB to store the data used for the sentences to keep track. I eventually decided against this idea as it would require an internet connection, and authentication to the database for the program to function. I then went on to learn to make better use of the HTTP requests to the server. To get stored sentences it gets it from the URL it was navigated to via the application's "GET" and "POST" methods. It navigates to a different URL containing the required data to display that page and its contents. Parts of the URL hold the data that is displayed in the view.